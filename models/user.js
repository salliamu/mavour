const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
    enum: ["admin", "vendor", "client"],
  },
  name: {
    firstName: { type: String },
    lastName: { type: String },
  },
  photo: {
    type: String,
  },
  about: {
    type: String,
  },
  contact: {
    phone_number: { type: String },
    address: { type: String },
    website: { type: String },
  },
  services: [
    {
      service_type: { type: String },
      service_price: { type: Number },
    },
  ],
  createdEvents: [
    {
      type: Schema.Types.ObjectId,
      ref: "Event",
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
