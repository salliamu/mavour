const { buildSchema } = require("graphql");

module.exports = buildSchema(`
type Event {
  _id: ID!
  title: String!
  description: String!
  creator: User!
}

type User {
  _id: ID!
  email: String!
  password: String 
  role: String!
  first_name: String 
  last_name: String
  photo: String
  about: String
  phone_number: String
  address: String
  website: String
  service_type : String,
  service_price: Float
  createdEvents: [Event!]
}

type AuthData {
  userId: ID!
  token: String!
  tokenExpiration: Int
}

input EventInput {
  title: String!
  description: String!
}

input UserInput {
  email: String!
  password: String!
  role: String!
}

type RootQuery {
    events: [Event!]!
    login(email: String!, password: String!): AuthData!   
}
type RootMutation {
    createEvent(eventInput: EventInput): Event
    createUser(userInput: UserInput) : User
}
schema {
    query: RootQuery
    mutation: RootMutation
}
`);
