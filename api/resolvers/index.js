const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Event = require("../../models/event");
const User = require("../../models/user");

module.exports = {
  events: () => {
    return Event.find()
      .then((events) => {
        return events.map((event) => {
          return {
            ...event._doc,
            _id: event.id,
          };
        });
      })
      .catch((err) => {
        throw err;
      });
  },

  createEvent: (args, req) => {
    if (!req.isAuth) {
      throw new Error("User is not authenticated!");
    }
    const event = new Event({
      title: args.eventInput.title,
      description: args.eventInput.description,
    });
    return event
      .save()
      .then((result) => {
        console.log(result);
        return { ...result._doc, _id: result.id };
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  },

  createUser: (args) => {
    // Ensure unique email addresses
    return User.findOne({ email: args.userInput.email })
      .then((user) => {
        if (user) {
          throw new Error("User already exists");
        }
        return bcrypt.hash(args.userInput.password, 12);
      })
      .then((hashedPassword) => {
        const user = new User({
          email: args.userInput.email,
          password: hashedPassword,
          role: args.userInput.role,
        });
        return user.save();
      })
      .then((result) => {
        // Overriding the retrieved password with null for security.
        return { ...result._doc, password: null, _id: result.id };
      })
      .catch((err) => {
        throw err;
      });
  },
  login: async ({ email, password }) => {
    const loginuser = await User.findOne({ email: email });
    if (!loginuser) {
      throw new Error("User does not exist");
    }
    const isEqual = await bcrypt.compare(password, loginuser.password);
    if (!isEqual) {
      throw new Error("Password is incorrect!");
    }
    // You can add data to the token
    const token = jwt.sign(
      { userId: loginuser.id, email: loginuser.email, role: loginuser.role },
      "thatsecretkey",
      {
        expiresIn: "1h",
      }
    );
    return { userId: loginuser.id, token: token, tokenExpiration: 1 };
  },
};
